<?php

/*
=====================================================
Sample Example Plugin for common tools/functions
=====================================================
*/

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$plugin_info = array(
		     'pi_name' => 'Common Tools',
		     'pi_version' =>'1.0',
		     'pi_author' =>'Carl Crawley',
		     'pi_author_url' => 'http://madebyhippo.com',
		     'pi_description' => 'Internal and Common Functions and Methods'
		     );

class Common_tools {

  function Common_tools() {

    	//--------------------------------------------
		//	Alias to get_instance() for backward compatibility
		//--------------------------------------------
		if ( ! function_exists('ee') )
		{
			function ee()
			{
				return get_instance();
			}
		}
	
	}
	
	//--------------------------------------------
	//	Start your functions here
	//--------------------------------------------
	
	function test_me() // Execute this with {exp:common_tools:test_me} in your template.
	{
		return 'It works';
	}
	

  }
